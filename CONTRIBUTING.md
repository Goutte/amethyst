# Contributing to Amethyst

Thank you for looking at this page! <3

There are many ways to contribute to Amethyst.

## Make this project known

- add a star on [the Gitlab repository](https://framagit.org/roipoussiere/amethyst);
- share this project on you favorite social media;
- talk about Amethyst to your family and friends.

## Provide feedback

Want to report a bug or suggest a feature?
Just [create a new issue](https://framagit.org/roipoussiere/amethyst/issues/new) on the Gitlab repository.

If you are not used to Gitlab, you can also [contact me on Mastodon](https://mastodon.tetaneutral.net/@roipoussiere).

## Be creative

In a future release, you will be able to create custom color palettes and modules, and share your work to other people.

Just wait a little bit. ;)

## Update documentation

### Minor updates

If you just want to fix a typo, you can use the GitLab built-in interface to directly edit a page and create a pull
request in order to let me include your modification in the next version of Amethyst.

Just select a file from [the project page on Gitlab](https://framagit.org/roipoussiere/amethyst/-/tree/master/)
and click on the *Edit* button. You must have an account on [Framagit](https://framagit.org/).

### Bigger updates

The documentation website is built with [mkDocs](https://www.mkdocs.org/), using the
[material theme](https://squidfunk.github.io/mkdocs-material/).

To install these Python dependencies in a Python virtual environment (you need Python installed on your system):

```
make docs-init
```

To build the documentation:

```
make docs
```

Or to both build and serve the documentation on a local website:

```
make docs-serve
```

## Contribute to code

### Install Golang

`sudo apt install golang` on Ubuntu. On other systems, please read the
[installation instructions](https://golang.org/doc/install).

### Install system dependencies

This is required by [GLFW](https://www.glfw.org/) to build source code.

Read [installation instructions](https://github.com/go-gl/glfw#installation)
(on Ubuntu: `sudo apt install libgl1-mesa-dev xorg-dev`).

### Write code

[Go by Example](https://gobyexample.com/) is a good start if your are new to Go.

### Install Amethyst for development

```
git clone https://framagit.org/roipoussiere/amethyst.git
cd amethyst
make init  # install Go dependencies
make  # build Amethyst sources
```

Then you can check your installation with `build/amy --version`.

## Developing

### Fork the project

1. [Fork the Amethys project](https://framagit.org/roipoussiere/amethyst/-/forks/new);
2. update the url of the *origin* remote (you can get it by clicking the *clone* button on your fork page on GitLab):

```
git remote set-url origin https://framagit.org/<your_gitlab_user>/amethyst.git
```

### Test your code

Several tools are available to help you to check your source code, which can be executed with make commands:

- `make check` executes [go vet](https://golang.org/cmd/vet/) to examine source code, and
[golint](https://pkg.go.dev/golang.org/x/lint/golint) to look for style mistakes in source code;
- `make format` executes [gofmt](https://golang.org/cmd/gofmt/) to format source code.

Notes:
- Golint must be installed before with `go get -u golang.org/x/lint/golint` (or `sudo apt install golint` on Ubuntu);
- any of these commands are executed in during the CI for now.

### Use Git Hooks

You may want to automatically executes these commands before each commit with a
[git hook](https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks):

```
echo 'make check' > .git/hooks/pre-commit
echo 'make format' >> .git/hooks/pre-commit
```

Notes:
- these commands are executed really fast and should not bother the developper.
- the hook will not block the commit (you must ammend it to fix the report).

### Create a merge request

Create a new git branch, write code, create commits, push to your fork then
[create a merge request](https://framagit.org/roipoussiere/amethyst/-/merge_requests/new)!
