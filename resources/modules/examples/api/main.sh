#!/usr/bin/env bash
# dependency: libnotify (ie. sudo apt install libnotify-bin)

json_path="`dirname "$0"`/gem.json"
json_oneliner=`cat "$json_path" | tr '\n' ' ' | sed 's/[[:space:]]\+/ /g'`
echo "$json_oneliner"

while read input; do
	if   [ "$input" == "N" ]; then facet="north"
	elif [ "$input" == "E" ]; then facet="east"
	elif [ "$input" == "S" ]; then facet="south"
	elif [ "$input" == "W" ]; then facet="west"
	else facet="unknown"
	fi
	notify-send "You selected the ${facet} facet."
done
