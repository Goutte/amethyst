.PHONY: build doc

SOURCE_DIR = ./amethyst
MAIN_FILE_PATH = ./main.go
SOURCE_DOC_DIR = ./doc
PUBLIC_DIR = ./public
BUILD_DIR = ./build
TARGET_DOC_DIR = $(BUILD_DIR)/doc
BIN_DIR = $(BUILD_DIR)/bin
PUBLIC_BIN_DIR = $(PUBLIC_DIR)/bin
PYTHON_VENV_DIR = ./.venv
MKDOCS_EXE = $(PYTHON_VENV_DIR)/bin/mkdocs

help:  ## Show this help message.
	@## https://gist.github.com/prwhite/8168133#gistcomment-1716694
	@echo "$$(grep -hE '^\S+:.*##' $(MAKEFILE_LIST) | sed -e 's/:.*##\s*/:/' -e 's/^\(.\+\):\(.*\)/\x1b[36m\1\x1b[m:\2/' | column -c2 -t -s :)" | sort

build:  ## Build Amethyst binary from sources.
	go build -o $(BIN_DIR)/amethyst $(SOURCE_DIR)

check:  ## Check source code with go vet and golint.
	go vet $(SOURCE_DIR)
	golint $(SOURCE_DIR)/*.go

format:  ## Format source code with gofmt.
	gofmt -w $(SOURCE_DIR)

pages:  ## Copy files to the public folder in order to publish the static website.
	rm -rf $(PUBLIC_DIR)
	mkdir -p $(PUBLIC_DIR)
	cp -r $(TARGET_DOC_DIR)/* $(PUBLIC_DIR)
	cp -r $(BIN_DIR) $(PUBLIC_BIN_DIR)

clean: ## Remove all files generated by make commands
	rm -rf $(PUBLIC_DIR)
	rm -rf $(BUILD_DIR)
	rm -f $(SOURCE_DOC_DIR)/README.md
	rm -f $(SOURCE_DOC_DIR)/CONTRIBUTING.md

doc-init:  ## Install Python dependencies required to build doc in a virtual env.
	python -m venv $(PYTHON_VENV_DIR)
	$(PYTHON_VENV_DIR)/bin/pip install mkdocs-material

doc:  # Build documentation
	cp ./CONTRIBUTING.md $(SOURCE_DOC_DIR)/CONTRIBUTING.md
	cp ./README.md $(SOURCE_DOC_DIR)/getting_started.md
	sed -i 's/\.\/doc\//\.\//g' $(SOURCE_DOC_DIR)/getting_started.md
	perl -i -0pe "s/<!-- hide -->(.*?)<!-- end-hide -->/<!-- \1 -->/mgs" $(SOURCE_DOC_DIR)/getting_started.md
	perl -i -ple "s/^(> )?(?!!|$$)/    / if s/^> /!!! note\n    /../^$$/" $(SOURCE_DOC_DIR)/getting_started.md
	rm -rf $(TARGET_DOC_DIR)
	mkdir -p $(TARGET_DOC_DIR)
	$(MKDOCS_EXE) build --strict

doc-serve: doc  ## Build doc and serve it on a local website.
	$(MKDOCS_EXE) serve

version:
	$(eval VERSION = $(shell git describe --tags))
	sed -i "s/^\tAppVersion string = \".*\"$$/\tAppVersion string = \"$(VERSION)\"/" $(SOURCE_DIR)/app_info.go
