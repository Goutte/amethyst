// instruction.go contains code related to modules instructions.

package main

import (
	"encoding/json"
)

// Instruction defines a gem instruction, usually sent by a module to update the gem.
type Instruction struct {
	// APIVersion string     `json:"api_version"`
	Commands   []ICommand `json:"commands"`
	GemAttrs   IGemAttrs  `json:"gem_attrs"`
	Facets     []Facet    `json:"facets"`
}

// IGemAttrs defines the gem attributes that can be defined in a module instruction.
type IGemAttrs struct {
	Mode IGMode `json:"mode"`
}

// ICommand lists the commands that can be defined in a module instruction.
type ICommand string

const (
	icQuit  ICommand = "quit"
	icClear ICommand = "clear"
)

func (instruction *Instruction) containsCommand(command ICommand) bool {
	for _, commandItem := range instruction.Commands {
		if commandItem == command {
			return true
		}
	}
	return false
}

// IGMode lists the gem types that can be defined in the module type attribute
type IGMode string

const (
	igmDuo    IGMode = "duo"
	igmQuadro IGMode = "quadro"
	igmOcto   IGMode = "octo"
)

// ParseInstruction parses a module instruction formated in json, and returns an Instruction struct.
func ParseInstruction(jsonString string) Instruction {
	var instruction Instruction
	err := json.Unmarshal([]byte(jsonString), &instruction)
	failOnError(err, "Can not parse module instruction.")
	return instruction
}
