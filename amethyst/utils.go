// utils.go is used to provide global utils functions for various needs.

package main

import (
	"fmt"
	"github.com/elastic/go-sysinfo"
	"github.com/kirsle/configdir"
	"github.com/otiai10/copy"
	"log"
	"os"
	"path"
	"runtime"
	"strings"
)

func getProjectPath() string {
	_, filename, _, ok := runtime.Caller(1)
	if !ok {
		fail("Can not get builtin config path.")
	}
	return path.Dir(path.Dir(filename))
}

// GetUserResourcePath returns the path of the given resource located in the user config folder.
func GetUserResourcePath(resourceName string) string {
	resourcesDir := configdir.LocalConfig(AppName)
	err := configdir.MakePath(resourcesDir)
	failOnError(err, "Can not make user resources path.")

	return path.Join(resourcesDir, resourceName)
}

// LogSysInfo logs various information about the host system, useful for debugging purpose.
func LogSysInfo() {
	host, err := sysinfo.Host()
	failOnError(err, "Can not get system info.")
	osInfo := host.Info()

	log.Printf("System information:")
	log.Printf("  • System architecture: %s\n", osInfo.Architecture)
	log.Printf("  • OS: %s %s %s\n", osInfo.OS.Family, osInfo.OS.Name, osInfo.OS.Version)
	log.Printf("  • Kernel version: %s\n", osInfo.KernelVersion)
	log.Printf("  • Go version: %s\n", runtime.Version())
	log.Printf("  • Amethyst version: %s\n", AppVersion)
}

// InitResources copies builtin resources into user's resources folder if doesn't exist.
func InitResources(resources ...string) {
	log.Println("Initializing resources " + strings.Join(resources, ", ") + "...")
	for _, resource := range resources {
		userResourcePath := GetUserResourcePath(resource)
		if _, err := os.Stat(userResourcePath); os.IsNotExist(err) {
			log.Printf("User's %v does not exist, creating it from builtin %v...\n", resource, resource)

			builtinResourcePath := path.Join(getProjectPath(), "resources", resource)
			err := copy.Copy(builtinResourcePath, userResourcePath)
			failOnError(err, "Can not copy builtin resources to "+userResourcePath+".")
		}
	}
}

// startLogging initialize and starts the logger.
func startLogging() *os.File {
	logPath := path.Join(os.TempDir(), "amethyst.log")
	file, err := os.OpenFile(logPath, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	failOnError(err, "Can not open log file")
	log.SetOutput(file)
	log.Println("=== Starting Amethyst ===")
	return file
}

func failOnError(err error, message string) {
	if err != nil {
		fmt.Println(message)
		log.Println("Fatal error: " + message)
		log.Fatalf("  ⮡ " + err.Error())
	}
}

func fail(message string) {
	fmt.Println("Error:", message)
	log.Fatalln("Error:", message)
}
