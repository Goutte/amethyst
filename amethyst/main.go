// main.go is the Amethyst app starting point that provides the amethyst command.

package main

import (
	"flag"
	"fmt"
	"github.com/faiface/pixel/pixelgl"
	"io"
	"log"
	"os"
	"os/exec"
)

const (
	usageExamples string = `examples:
  amethyst examples.greetings                  # call module by identifier
  amethyst ./examples/greetings/main.sh        # call module by path
  cat examples/input/simple.json | amethyst -  # read from stdin and outputs to stdout`
)

func initialize() {
	InitResources("config.yml", "modules", "color_schemes")
	LogSysInfo()

	// var config AppConfig = getConfig()
	// fmt.Println("WindowSize: ", config.WindowSize)
}

func start(moduleArg string) {
	log.Println("Launching module " + moduleArg)
	var stdin io.ReadCloser
	var stdout io.WriteCloser

	if moduleArg == "-" {
		stdin, stdout = os.Stdin, os.Stdout
	} else {
		var cmd *exec.Cmd
		stdin, stdout, cmd = startModule(moduleArg)
		defer cmd.Wait()
	}

	instructionChan := make(chan Instruction)
	userInputChan := make(chan UserInput)

	log.Println("Starting GUI")
	go readFromStdin(instructionChan, stdin)
	go writeToStdout(userInputChan, stdout)
	go gui(instructionChan, userInputChan)
}

func parseCli() {
	flagVersion := flag.Bool("version", false, "Show version and exit.")
	flagVerbose := flag.Bool("verbose", false, "Verbose mode.")
	flag.Parse()

	if !*flagVerbose {
		file := startLogging()
		defer file.Close()
	}

	log.Println("=== Starting Amethyst ===")
	log.Println("Parsed CLI arguments:")
	flag.VisitAll(func(flag *flag.Flag) {
		log.Printf("  • %s: %s\n", flag.Name, flag.Value)
	})

	if *flagVersion {
		fmt.Println(AppVersion)
	}

	if len(flag.Args()) != 1 {
		fmt.Println("Usage: amethyst <module name|script path>: \n" + usageExamples)
		os.Exit(1)
	}

	initialize()
	start(flag.Arg(0))
}

func main() {
	pixelgl.Run(parseCli)
}
