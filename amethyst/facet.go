// facet.go contains code related to the Facet struct.

package main

// Facet represents a gem facet, that can be chosed by the user.
type Facet struct {
	Position FacetPosition `json:"pos"`
	Label    string        `json:"label"`
}

// FacetPosition represents the position of a facet in the gem (north, south, etc.).
type FacetPosition string

const (
	fpNorth     FacetPosition = "N"
	fpNorthEast FacetPosition = "NE"
	fpEast      FacetPosition = "E"
	fpSouthEast FacetPosition = "SE"
	fpSouth     FacetPosition = "S"
	fpSouthWest FacetPosition = "SW"
	fpWest      FacetPosition = "W"
	fpNorthWest FacetPosition = "NW"
)

var (
	octoGemPos [8]FacetPosition = [8]FacetPosition{fpNorth, fpNorthEast, fpEast, fpSouthEast,
fpSouth, fpSouthWest, fpWest, fpNorthWest}
)

// NewFacet creates a new facet at a given position, with "∅" as label.
func NewFacet(position FacetPosition) Facet {
	return Facet{position, "∅"}
}

// ToString returns the string reprensentation of a facet.
func (facet *Facet) ToString() string {
	if facet == nil {
		return "∅"
	}
	return string(facet.Position) + ": " + facet.Label
}
