// gui.go contains code related to the Graphical User Interface.

package main

import (
	"image"
	"log"
	"math"

	"github.com/faiface/pixel"
	"github.com/faiface/pixel/pixelgl"
	"github.com/llgcode/draw2d"
	"github.com/llgcode/draw2d/draw2dimg"
	"golang.org/x/image/colornames"
	"image/color"
)

// UserInput defines the actions that can be triggered by the user.
type UserInput string

const (
	uiUp    UserInput = "u"
	uiRight UserInput = "r"
	uiDown  UserInput = "d"
	uiLeft  UserInput = "l"
	uiClose UserInput = "c"
	uiNone  UserInput = ""
)

const (
	winWidth, winHeight      float64 = 400, 400
	innerRadius, outerRadius float64 = 50, 150
	gap                      float64 = 10
)

func gui(instructionChan chan Instruction, userInputChan chan UserInput) {
	pxWindow := createWindow()

	log.Println("Entering GUI loop")
	for !pxWindow.Closed() {
		userInput := getUserInput(pxWindow)
		if userInput != uiNone {
			userInputChan <- userInput
		}

		select {
		case instruction := <-instructionChan:
			drawOnWindow(pxWindow, instruction)
		default:
		}
		pxWindow.Update()
	}
	log.Println("Quitted GUI loop")
	userInputChan <- uiClose
}

func createWindow() *pixelgl.Window {
	log.Println("Creating window")
	var monitorWidth, monitorHeight float64 = pixelgl.PrimaryMonitor().Size()

	config := pixelgl.WindowConfig{
		Title:                  "Amethyst",
		Bounds:                 pixel.R(0, 0, winWidth, winHeight),
		VSync:                  true,
		Resizable:              false,
		Undecorated:            true,
		TransparentFramebuffer: true,
	}

	win, err := pixelgl.NewWindow(config)
	failOnError(err, "Can not create window")

	win.SetPos(pixel.V(monitorWidth/2-winWidth/2, monitorHeight/2-winHeight/2))
	return win
}

func getUserInput(win *pixelgl.Window) UserInput {
	key := uiNone
	if win.JustPressed(pixelgl.KeyLeft) {
		key = uiLeft
	} else if win.JustPressed(pixelgl.KeyRight) {
		key = uiRight
	} else if win.JustPressed(pixelgl.KeyUp) {
		key = uiUp
	} else if win.JustPressed(pixelgl.KeyDown) {
		key = uiDown
	}
	if key != "" {
		log.Println("Pressed key " + key)
	}
	return key
}

func drawOnWindow(pxWindow *pixelgl.Window, instruction Instruction) {
	log.Print("Updating window content")
	nbFacetsbyMode := map[IGMode]int{igmDuo: 2, igmQuadro: 4, igmOcto: 8}

	image := pixel.PictureDataFromImage(createGemImage(nbFacetsbyMode[instruction.GemAttrs.Mode]))
	sprite := pixel.NewSprite(image, image.Bounds())
	pxWindow.Clear(color.RGBA{0x00, 0x00, 0x00, 0x00})
	sprite.Draw(pxWindow, pixel.IM.Moved(pxWindow.Bounds().Center()))
}

func createGemImage(nbFacets int) image.Image {
	const cx, cy float64 = winWidth / 2, winHeight / 2

	img := image.NewRGBA(image.Rect(0, 0, int(winWidth), int(winHeight)))
	gc := draw2dimg.NewGraphicContext(img)
	gc.BeginPath()
	gc.SetFillColor(colornames.Grey)

	outerGapAngle := math.Asin(gap / 2 / outerRadius)
	innerGapAngle := math.Asin(gap / 2 / innerRadius)
	angle := 2 * math.Pi / float64(nbFacets)

	for i := 0; i < nbFacets; i++ {
		startAngle := float64(i) * angle
		path := new(draw2d.Path)
		path.ArcTo(cx, cy, outerRadius, outerRadius, startAngle-angle/2+outerGapAngle, angle-2*outerGapAngle)
		path.ArcTo(cx, cy, innerRadius, innerRadius, startAngle+angle/2-innerGapAngle, -angle+2*innerGapAngle)
		path.Close()
		gc.Fill(path)
	}

	return img
}
