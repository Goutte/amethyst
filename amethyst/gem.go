// gem.go contains code related to the Gem struct.

package main

import (
	"bufio"
	"io"
	"strings"
)

// Gem represents an the radial menu content and composed into many facets.
type Gem struct {
	north, northEast, east, southEast, south, southWest, west, northWest Facet
}

// NewGemFromMap creates a new gem composed of facets defined in the given map, filling with new facets if necessary.
func NewGemFromMap(facetsMap map[FacetPosition]Facet) Gem {
	for _, facetPosition := range []FacetPosition{fpNorth, fpNorthEast, fpEast, fpSouthEast,
fpSouth, fpSouthWest, fpWest, fpNorthWest} {
		if _, isPresent := facetsMap[facetPosition]; !isPresent {
			facetsMap[facetPosition] = NewFacet(facetPosition)
		}
	}
	return Gem{facetsMap[fpNorth], facetsMap[fpNorthEast], facetsMap[fpEast], facetsMap[fpSouthEast],
		facetsMap[fpSouth], facetsMap[fpSouthWest], facetsMap[fpWest], facetsMap[fpNorthWest]}
}

// NewGem creates a new gem composed of given facets, filling with new facets if necessary.
func NewGem(facets ...Facet) Gem {
	facetsMap := make(map[FacetPosition]Facet)
	for _, facet := range facets {
		facetsMap[facet.Position] = facet
	}
	return NewGemFromMap(facetsMap)
}

// ToMap returns a map of FacetPosition:Facet, given a gem structure.
func (gem *Gem) ToMap() map[FacetPosition]Facet {
	return map[FacetPosition]Facet{
		fpNorth: gem.north, fpNorthEast: gem.northEast, fpEast: gem.east, fpSouthEast: gem.southEast,
		fpSouth: gem.south, fpSouthWest: gem.southWest, fpWest: gem.west, fpNorthWest: gem.northWest,
	}
}

// ToString returns the string reprensentation of a gem.
func (gem *Gem) ToString() string {
	if gem == nil {
		return "∅"
	}
	var stringFacets []string
	for _, facet := range gem.ToMap() {
		stringFacets = append(stringFacets, facet.ToString())
	}
	return strings.Join(stringFacets, " | ")
}

// FromDmenu returns a gem structure containing the facet listed in the given reader in dmenu syntax.
func FromDmenu(reader io.Reader) Gem {
	scanner := bufio.NewScanner(reader)
	var facets []Facet
	for i := 0; scanner.Scan(); i++ {
		facets = append(facets, Facet{octoGemPos[i], scanner.Text()})
	}
	failOnError(scanner.Err(), "Can not scan stdin")
	return NewGem(facets...)
}
