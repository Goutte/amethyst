// module.go contains code related to the modules.

package main

import (
	"bufio"
	"io"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"strings"
)

// Return the absolute path of a given module
func getCmdPathFromModuleName(moduleName string) string {
	moduleRelativeDir := path.Join(strings.Split(moduleName, ".")...)
	moduleDir := path.Join(GetUserResourcePath("modules"), moduleRelativeDir)

	files, err := ioutil.ReadDir(moduleDir)
	failOnError(err, "Can not find module directory "+moduleDir)

	for _, f := range files {
		if strings.TrimSuffix(f.Name(), filepath.Ext(f.Name())) == "main" {
			absPath, err := filepath.Abs(path.Join(moduleDir, f.Name()))
			failOnError(err, "Can not get absolute path of "+f.Name()+" in "+moduleDir+".")
			return absPath
		}
	}
	fail("Can not find main script in module directory " + moduleDir)
	return ""
}

func getCmdPathFromModuleArg(moduleArg string) string {
	var moduleCmdPath string
	var err error
	if moduleArg[0:1] == "." || moduleArg[0:1] == string(os.PathSeparator) {
		moduleCmdPath, err = filepath.Abs(moduleArg)
		failOnError(err, "Can not get module absolute path of '"+moduleArg+"'.")
	} else {
		moduleCmdPath = getCmdPathFromModuleName(moduleArg)
	}
	log.Println("Module executable path: " + moduleCmdPath)
	return moduleCmdPath
}

func startModule(modulePath string) (io.ReadCloser, io.WriteCloser, *exec.Cmd) {
	var stdin io.ReadCloser
	var stdout io.WriteCloser
	var err error

	cmd := exec.Command(getCmdPathFromModuleArg(modulePath))

	cmd.Stderr = os.Stderr

	stdin, err = cmd.StdoutPipe()
	failOnError(err, "Can not obtain module stdin")

	stdout, err = cmd.StdinPipe()
	failOnError(err, "Can not obtain module stdout")

	log.Println("Starting Module")
	err = cmd.Start()
	failOnError(err, "Error starting module")

	return stdin, stdout, cmd
}

func readFromStdin(instructionChan chan Instruction, stdin io.ReadCloser) {
	scanner := bufio.NewScanner(bufio.NewReader(stdin))
	for scanner.Scan() {
		instructionJson := scanner.Text()
		log.Printf("Read from module: %s\n", instructionJson)

		instruction := ParseInstruction(instructionJson)
		log.Printf("Parsed instruction: %s\n", instruction)

		if instruction.containsCommand(icQuit) {
			break
		}
		instructionChan <- instruction
	}
	log.Println("Closing stdin")
	stdin.Close()
}

func writeToStdout(userInputChan chan UserInput, stdout io.WriteCloser) {
	for userInput := range userInputChan {
		if userInput == uiClose {
			stdout.Close()
			break
		}

		inputToFacet := map[UserInput]FacetPosition{uiUp: fpNorth, uiRight: fpEast, uiDown: fpSouth, uiLeft: fpWest}
		output := string(inputToFacet[userInput])

		log.Println("Sending to stdout: " + output)
		stdout.Write([]byte(output + "\n"))
	}
}
