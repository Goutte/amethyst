# Amethyst

*A general-purpose keyboard-centric radial menu.*

<!-- hide -->
More complete instructions about Amethyst is available on [the documentation website](https://roipoussiere.frama.io/amethyst/).
<!-- end-hide -->

> This is a work in progress, don't expect anything interesting for now.
> You probably want to bookmark this page and to come back later. ;)

## Installation

Installation process will be improved later. Also, keep it mind that this project is still under development.

### Download binary

An executable of current development version of Amethyst is built on each project update:

- [Linux](https://roipoussiere.frama.io/amethyst/bin/amethyst)

Windows and MacOS executables are not available for now, please install with the Go command as described above.

### With Go command (recommended)

This procedure is recommended for multi-platform support, since it builds Amethyst from sources.

First install Go. `sudo apt install golang` on Ubuntu. On other systems, please read the
[installation instructions](https://golang.org/doc/install).

```
go get framagit.org/roipoussiere/amethyst
```

## Usage

### Using dmenu syntax

It is the only syntax available for now.

**From stdin**

```
cat examples/dmenu1 | amy --dmenu -
```

**From file**

```
amy --dmenu examples/dmenu1
```

## Build from source

```
go build -o ./amy ./amethyst
```


## Contributing

The [contribution guide](./CONTRIBUTING.md) is waiting for you!

## Credits

- author: Nathanaël Jourdane and contributors ([you?](./CONTRIBUTING.md))
- license: [MIT](./LICENSE)
