module framagit.org/roipoussiere/amethyst

go 1.13

require (
	github.com/elastic/go-sysinfo v1.4.0
	github.com/faiface/pixel v0.10.0-beta
	github.com/kirsle/configdir v0.0.0-20170128060238-e45d2f54772f
	github.com/llgcode/draw2d v0.0.0-20200603164053-19660b984a28
	github.com/otiai10/copy v1.2.0
	golang.org/x/image v0.0.0-20200801110659-972c09e46d76
	gopkg.in/yaml.v2 v2.3.0
)
